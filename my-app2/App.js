

import React, { useEffect, useState } from "react";
import axios from "axios";
import { Dimensions, ScrollView, StyleSheet, Text, View } from "react-native";
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart
} from 'react-native-chart-kit'
const API_URL = "http://prometheus-route-montamonta-dev.apps.sandbox-m3.1530.p1.openshiftapps.com/api/v1/query";

//"https://ez-learn-montamonta-dev.apps.sandbox-m3.1530.p1.openshiftapps.com/metrics/api/v1/query";
  //"https://prometheus-route-fyf2-0-dev.apps.sandbox-m2.ll9k.p1.openshiftapps.com/api/v1/query";
  //"http://ez-learn-monta619-dev.apps.sandbox-m3.1530.p1.openshiftapps.com/metrics";


export default function App() {
  const [memoryUsed, setMemoryUsed] = useState([]);
  const [gcCount, setGcCount] = useState([]);
  const [gcTime, setGcTime] = useState([]);
  
  const fetchData = (query, setValue,isRounded) => {
    const time = new Date().toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });

    axios
      .get(API_URL, { params: { query } })
      .then((res) => {
        if(isRounded){
        const value = Math.round(res.data.data.result[0].value[1]); 
        setValue((prevValue) => [...prevValue, { time, value }]);}
        else{
          const value = parseFloat(res.data.data.result[0].value[1]); 
          setValue((prevValue) => [...prevValue, { time, value }]);
        }
      })
      .catch((err) => console.log(err));
  };
  const getData = (data) => {
    const slicedData = data.slice(-7); // get last 5 elements
    return {
      labels: slicedData.map(d => d.time),
      datasets: [
        {
          data: slicedData.map(d => d.value),
          strokeWidth: 2,
        },
      ],
    };
  };
  useEffect(() => {
    const interval = setInterval(() => {
      fetchData("jvm_memory_bytes_used", setMemoryUsed,true);
      fetchData("jvm_gc_collection_seconds_count", setGcCount,true);
      fetchData("process_cpu_seconds_total", setGcTime,false);
    }, 5000);

    return () => clearInterval(interval);
  }, []);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.label}>Memory Used:</Text>
      {memoryUsed.length!=0&&
      <LineChart
  data={getData(memoryUsed)}
  width={Dimensions.get('window').width}
  height={220}
  yAxisLabel={"MB"}
  chartConfig={{
    backgroundColor: '#e26a00',
    backgroundGradientFrom: '#fb8c00',
    backgroundGradientTo: '#ffa726',
    decimalPlaces: 0, 
    color: (opacity = 1) => "rgba(255, 255, 255, 1)",
    style: {
      borderRadius: 16
    },
    xAxis: {
      labelRotationAngle: 0, 
    },
    yAxis: {
      labelRotationAngle: 45, 
    },
  }}
  bezier
  style={{
    marginVertical: 8,
    borderRadius: 16
  }}
/>}
      <Text style={styles.label}>GC Count:</Text>
     {gcCount.length!=0&& <LineChart
  data={getData(gcCount)}
  width={Dimensions.get('window').width}
  height={220}
  yAxisLabel={"MB"}
  chartConfig={{
    backgroundColor: '#e26a00',
    backgroundGradientFrom: '#fb8c00',
    backgroundGradientTo: '#ffa726',
    decimalPlaces: 0, // remove decimal points
    color: (opacity = 1) => "rgba(255, 255, 255, 1)",
    style: {
      borderRadius: 16
    },
    xAxis: {
      labelRotationAngle: 0, 
    },
    yAxis: {
      labelRotationAngle: 45, 
    },
  }}
  bezier
  style={{
    marginVertical: 8,
    borderRadius: 16
  }}
/>}
      <Text style={styles.label}>GC Time:</Text>
     
      {gcTime.length!=0 &&<LineChart
  data={getData(gcTime)}
  width={Dimensions.get('window').width}
  height={220}
  yAxisLabel={"MB"}
  chartConfig={{
    backgroundColor: '#e26a00',
    backgroundGradientFrom: '#fb8c00',
    backgroundGradientTo: '#ffa726',
    decimalPlaces: 0, // remove decimal points
    color: (opacity = 1) => "rgba(255, 255, 255, 1)",
    style: {
      borderRadius: 16
    },
    xAxis: {
      labelRotationAngle: 0, 
    },
    yAxis: {
      labelRotationAngle: 45, 
    },
  }}
  bezier
  style={{
    marginVertical: 8,
    borderRadius: 16
  }}
/>}
{gcTime.length!=0 &&<LineChart
  data={getData(gcTime)}
  width={Dimensions.get('window').width}
  height={220}
  yAxisLabel={"%"}
  chartConfig={{
    backgroundColor: '#e26a00',
    backgroundGradientFrom: '#fb8c00',
    backgroundGradientTo: '#ffa726',
    color: (opacity = 1) => "rgba(255, 255, 255, 1)",
    style: {
      borderRadius: 16
    },
    xAxis: {
      labelRotationAngle: 0, 
    },
    yAxis: {
      labelRotationAngle: 45, 
    },
  }}
  bezier
  style={{
    marginVertical: 8,
    borderRadius: 16
  }}
/>}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    width:"95%",
    alignSelf:"center",
    marginTop:10,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  label: {
    fontSize: 16,
    fontWeight: "bold",
    marginTop: 10,
  },
});





